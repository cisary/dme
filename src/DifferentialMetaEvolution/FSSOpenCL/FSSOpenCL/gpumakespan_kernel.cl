kernel void gpumakespan(
                         global float *operation,
                         global float *a,
                         global float *b,
                         global float *out)
{
    
    size_t i = get_global_id(0);
    a[i] = operation[i] * operation[i];
    
}