//
//  InputView.m
//  IntroToQuartzPartTwo
//

//  This code is based on the "Introduction to Quartz II" tutorial at:
//  <http://cocoadevcentral.com/d/intro_to_quartz_two/>
//  by Scott Stevenson on 11/27/06.
//
//  Personal site: http://theocacao.com/
//  Email: sstevenson@mac.com


#import "InputView.h"

@interface InputView (Examples)
- (void)drawExample1;		// curved line
- (void)drawExample2;		// partial oval
- (void)drawExample3;		// rectangle
- (void)drawExample4;		// Taijitu
- (void)drawExample5;		// single image
- (void)drawExample6;		// images with transparency
- (void)drawExample7;		// drawing into offscreen image


- (NSImage *)selected;
@end

@interface InputView (Private)
- (void)configureExampleImages;
@end

@interface InputView (Accessors)
- (NSImage *)imageForExample1;
@end


@implementation InputView

- (id)initWithFrame:(NSRect)frame
{
    if ( self = [super initWithFrame:frame] ) {
		_imageForExample1 = nil;
    }
    return self;
}

- (void)dealloc
{
	[self setImageForExample1:nil];
	[super dealloc];	
}

- (void)awakeFromNib
{
	// this loads the images for examples 5 and 6
	[self configureExampleImages];
}


#pragma mark -
#pragma mark Standard NSView Methods

- (void) drawRect: (NSRect)rect
{
	NSString * exampleName = [self selectedExample];
	
	if ( exampleName == nil || [exampleName isEqualToString:@""] )
	{
		// if exampleName is nil for some reason, we don't want to just
		// crash. Instead, we'll go to a reasonable default.
		
		[self drawExample1];
		
	} else {
		
		// The "selectedExample" string is bound to the arrayController.
		// When the user selects a new item from the example dropdown
		// in the UI, the selectedExample variable in this class is
		// updated.
		//
		// The -setSelectedExample method in this class calls:
		// [self setNeedsDisplay:YES] which triggers a redraw, calling
		// this drawRect method.
		//
		// Once we get here, we take the selected example name, stick
		// "draw" on the front, then call the result as a method:
		//
		// 		"draw" + "Example4" = "drawExample4"
		//
		// We convert the string to an Objective-C selector with
		// NSSelectorFromString(), then call -performSelector:

		exampleName = [@"draw" stringByAppendingString:exampleName];		
		SEL selector = NSSelectorFromString( exampleName );
		[self performSelector:selector];
	}

	// draw a solid line at bottom of the view. If we don't do this,
	// the view just sort of "ends," which looks really weird. we
	// use NSMaxY and NSMaxX to get the maximum X and Y axis values
	// for the "bounds" rect, which is our drawing area.
	
	NSRect bounds = [self bounds];	
	NSBezierPath *path = [NSBezierPath bezierPath];	
	[path moveToPoint: NSMakePoint ( 0, NSMaxY(bounds) )];
	[path lineToPoint: NSMakePoint ( NSMaxX(bounds),NSMaxY(bounds) )];	
	[[NSColor grayColor] set];
	[path stroke];
}

- (BOOL)isFlipped
{
	// Converts our coordinate system to "flipped," which means the
	// coordinates start in the upper-left, instead of the
	// lower-left. Keep in mind this means we have to call
	// setFlipped:YES on all images or they'll be upside-down.
		
	return YES;
}


#pragma mark -
#pragma mark Examples

-(void)drawImage:(int) i{
    // This example draws a single NSImage in the center
	// of the view and strokes a border around the edge.
    
	// fill the background with gray
	[[NSColor grayColor] set];
	NSRectFill ( [self bounds] );
    
	// set image interpolation to high for smooth scaling
	[[NSGraphicsContext currentContext]
     setImageInterpolation: NSImageInterpolationHigh];
    
	// find the center of the view and define the target
	// image size
	NSSize viewSize  = [self bounds].size;
	NSSize imageSize = { 250, 156 };
    
	NSPoint viewCenter;
	viewCenter.x = viewSize.width  * 0.50;
	viewCenter.y = viewSize.height * 0.50;
    
	// center the image by substracting 50 percent
	// of its width and height from its origin
	NSPoint imageOrigin = viewCenter;
	imageOrigin.x -= imageSize.width  * 0.50;
	imageOrigin.y -= imageSize.height * 0.50;
    
	// construct a rect to draw into using the size
	// and origin
	NSRect destRect;
	destRect.origin = imageOrigin;
	destRect.size = imageSize;
    
	// These lines of code were used on the website to illustrate
	// inefficiency. We're uncommenting them here and using the
	// proper technique.
	//
	// NSString * file = @"/Library/Desktop Pictures/Plants/Leaf Curl.jpg";
	// NSImage * image = [[NSImage alloc] initWithContentsOfFile:file];
    
	// Grab the the image and make sure it's flipped. This image is loaded
	// in -configureExampleImages, which is called in -awakeFromNib.
    
    NSImage * image;
    
    if (i==1)
        image = [self imageForExample1];
    
	//[image setFlipped:YES];
    
	// Draw into the destination rect. We say draw "from" NSZeroRect as
	// a convenient way of triggering the default behavior (which is
	// draw the whole image). If you wanted to draw only a portion of
	// the image, you could use a different rect as the "fromRect".
	//
	// NSCompositeSourceOver is the most common compositing mode. It
	// essentially means "draw this image over whatever is underneath,
	// respecting any alpha or transparency".
	//
	// Keep in mind there are also -compositeToPoint and -dissolveToPoint
	// methods, but they require a bit more setup. In most cases,
	// -drawInRect is the one you want
    
	[image drawInRect: destRect
	         fromRect: NSZeroRect
	        operation: NSCompositeSourceOver
	         fraction: 1.0];
    
	// how we're just going to draw a 3 unit white border around the
	// outside of the image
	NSBezierPath * path = [NSBezierPath bezierPathWithRect:destRect];
	[path setLineWidth:3];
	[[NSColor whiteColor] set];
	[path stroke];
    
	// No need to release this since we disabled the creation
	// code above
	//
	// [image release];
}

- (void)drawExample1
{   
	[self drawImage:1];
}

- (void)drawExample2
{
	[self drawImage:2];
}

- (void)drawExample3
{
	[self drawImage:3];
}

- (void)drawExample4
{
	[self drawImage:4];
}

- (void)drawExample5
{
	[self drawImage:5];
}

- (void)drawExample6
{
	[self drawImage:6];
}

- (void)drawExample7
{
	[self drawImage:7];
}

- (void)drawExample8
{
	[self drawImage:8];
}

#pragma mark -
#pragma mark Private Methods


- (void)configureExampleImages
{
	NSString * file = @"/Users/cisary/Pictures/black.jpg";
	NSImage * image = [[NSImage alloc] initWithContentsOfFile:file];	
	[self setImageForExample1:image];
	[image release];	
}



#pragma mark -
#pragma mark Accessors

- (NSImage *)imageForExample1
{
	return _imageForExample1;
}

- (void)setImageForExample1:(NSImage *)aValue
{
	NSImage *oldImageForExample1 = _imageForExample1;
	_imageForExample1 = [aValue copy];
	[oldImageForExample1 release];
}


- (NSString *)selectedExample
{
	return _selected;
}

- (void)setSelected:(NSString *)aValue
{
	NSString *oldSelectedExample = _selected;
	_selected = [aValue copy];
	[oldSelectedExample release];

	// since we have a new selected example, we need to redraw
	[self setNeedsDisplay:YES];
}

@end