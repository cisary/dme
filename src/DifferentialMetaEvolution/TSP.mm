
/*!
 * @brief Asymmetric Traveling Salesman Problem
 * @author doc. MSc. Donald Davendra Ph.D.
 * @date 3.10.2013
 *
 * This is a simple class to calculate the cost of a TSP tour.
 */

#include "TSP.h"

#include <iostream>
#include <fstream>

using namespace std;

cTSP::cTSP(){
    
    ifstream infile;
    char fname[120]="ft70.txt";
    infile.open(fname);
    
    if(!infile.is_open()) {
        cout << "Error Opening File.\n";
        exit(1);
    }
    else {
        infile >> m_Cities;
        
        m_Distance = new float[m_Cities * m_Cities];
        
        for (int i=0; i<(m_Cities * m_Cities); i++) {
            infile >> m_Distance[i];
        }
    }
    
    infile.close();
}

cTSP::~cTSP(){
    delete [] m_Distance;
}

int cTSP::GetCities(){
    
    return m_Cities;
}

float cTSP::TourCost(int *Tour){
    
    float cost = 0;
    
    for (int i=1; i<m_Cities; i++)
        cost += m_Distance[((Tour[i-1]-1)*m_Cities)+(Tour[i]-1)];
    
    return cost;
}

//! Function to calculate a simple tour.
/*!
 \return no return value
 */
void cTSP::TourFunction(cTSP *TSP){
    //! Initilaize a tour
    int *Tour = new int[TSP->GetCities()];
    
    //! Fill the schedule sequentially
    for (int i = 0; i < TSP->GetCities(); i++) {
        Tour[i] = i+1;
    }
    
    //! Cacluate the tour cost of the simple tour and display it in standard output
    cout << "The cost of the tour is: " << TSP->TourCost(Tour) << endl;
    
    //! Delete the schdule.
    delete [] Tour;
}


/*
//! Function to calculate a simple tour.

void cTSP::TourFunction(cTSP *TSP){
    //! Initilaize a tour
    int *Tour = new int[TSP->GetCities()];
    
    //! Fill the schedule sequentially
    for (int i = 0; i < TSP->GetCities(); i++) {
        Tour[i] = i+1;
    }
    
    //! Cacluate the tour cost of the simple tour and display it in standard output
    cout << "The cost of the tour is: " << TSP->TourCost(Tour) << endl;
    
    //! Delete the schdule.
    delete [] Tour;
}*/

