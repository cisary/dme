//
//  MersenneTwisterTests.m
//  MersenneTwisterTests
//
//  Created by Michal Cisarik on 10/12/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MersenneTwister.h"

@interface MersenneTwisterTests : XCTestCase {
    MersenneTwister *mersenetwister;
}

@end

@implementation MersenneTwisterTests

- (void)setUp
{
    [super setUp];
    mersenetwister=[[MersenneTwister alloc]init];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testExample
{
    unsigned int i=[mersenetwister randomUInt32From:0 to:10];
    XCTAssertTrue((i>0)&&(i<10));
}

@end
