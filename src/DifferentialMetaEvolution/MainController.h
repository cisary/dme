#import <Cocoa/Cocoa.h>
#import <CorePlot/CorePlot.h>

@interface MainController : NSObject<CPTPlotDataSource> {
    IBOutlet CPTGraphHostingView *hostView;
    CPTXYGraph *graph;
    NSArray *plotData;
}

// PDF / image export
//-(IBAction)exportToPDF:(id)sender;
//-(IBAction)exportToPNG:(id)sender;

@end

