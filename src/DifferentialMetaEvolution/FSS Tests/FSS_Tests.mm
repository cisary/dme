//
//  FSS_Tests.m
//  FSS Tests
//
//  Created by Michal Cisarik on 10/22/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <XCTest/XCTest.h>

#include "FSS.h"

#include <iostream>
#include <fstream>

using namespace std;

ifstream in("in.txt");
ofstream out("out.txt");

//! Function to calculate a simple schedule.
/*!
 \return makespan
 */
float JobMakespan(cFSS *FSS){
    //! Initilaize a schedule
    int *Schedule = new int[FSS->GetJobs()];
    
    //! Fill the schedule sequentially
    for (int i = 0; i < FSS->GetJobs(); i++) {
        Schedule[i] = i+1;
    }
    //! Cacluate the makespan of the simple schedule and display it in standard output
    //cout << "The makespan is: " << FSS->Makespan(Schedule) << endl;
    
    float out = FSS->Makespan(Schedule);
    
    //! Delete the schdule.
    delete [] Schedule;
    
    return out;
}

@interface FSS_Tests : XCTestCase

@end

@implementation FSS_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    //! Initialization of the FSS class
    cFSS* fss =new cFSS();
    
    //! Calculate a simple tour
    NSLog(@"Makespan = %.0f\n\n",JobMakespan(fss));
    
    int br=0;
}

@end
