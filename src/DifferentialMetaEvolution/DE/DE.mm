/*! \file DE.mm
 \brief A TSP header file.
 */

#import "DE.h"

@implementation DE

// constants:
static const int METAEVOLUTION_ITERATIONS = 1;

static const int EVOLUTION_ITERATIONS = 1;

static const int TSP_BEST_FITNESS_EVER = 10757;

static const int FSS_BEST_FITNESS_EVER = 1000;

static const int GENERATIONS_WITH_NO_CHANGE_RESETTING_MIGRATION = 5;

static const int AT_LEAST_IMPROVEMENTS_RESETTING_MIGRATION = 5;

static const int ALL_TO_ALL_MIGRATIONS = 5;

// properties:
@synthesize averageFitness,bestFitnesses,metaEvolutionIndividual,migratingVectors,migratingbuffer,bestbuffer,bestVectorFitness,repaired;

//! class method - calling alloc so [DE alloc] is not needed by user thanks to ARC
+ (id) newWithVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c defaultProblem:(Problem)p{
    
    return [[DE alloc] initWithVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c defaultProblem:(Problem)p];
}

//! seed mersenne twister random generator with current time
-(void)seed{
    mersennetwister=[mersennetwister initWithSeed:(unsigned int)time(NULL)];
}

//! init Xu vector - upper bound accoring to number of cities
-(void)initXu{
    
    [self seed];
    float upper;
    
    if (problem == TSP)
        upper = tsp->GetCities()-1;
    
    if (problem == FSS)
        upper = fss->GetJobs()-1;
    
    for ( vector = 0; vector < dimension; vector++ )
        Xu[vector] = upper ;
}

//! init Xl vector as 0
-(void)initXl{
    [self seed];
    for ( vector = 0; vector < dimension; vector++ )
        Xl[vector] = 0;
}

-(void)resetMigrate{
    
    [self randomVectors];
    [self randomRepairing];
    
    //buffer=[NSMutableString stringWithString:@""];
    
    for (int v = 0; v < dimension; v++) {
        vectors[0][v] = (double)migratingbuffer[v];
        repairing[0][v] = repairingbuffer[v];
        
        //[buffer appendString:[NSString stringWithFormat:@"%i,",migratingbuffer[v]]];
    }
    
    //NSLog(@"RESETTING AND MIGRATING INDIVIDUAL {%@}\n",buffer);
}

-(void)randomVectors{
    
    [self seed];
    
    for ( vector = 0; vector < numberVectors; vector++ ) {
        
        for (i = 0; i < dimension; i++)
            vectors[vector][i]=Xl[i]+(Xu[i]-Xl[i])*[mersennetwister randomDoubleFrom:0 to:1];
        
        for (j = 0; j < dimension; j++)
            
            while ((vectors[vector][j] < Xl[j]) || (vectors[vector][j] > Xu[j]))
                
                if (vectors[vector][j] < Xl[j]) {
                    vectors[vector][j] = 2 * Xl[j] - vectors[vector][j];
                    
                    if (vectors[vector][j] > Xu[j])
                        vectors[vector][j] = 2 * Xu[j] - vectors[vector][j];
                }
    }
}

//! generate random metaevolution (repairing) array 'repairing[][]'
-(void)randomRepairing{
    
    [self seed];
    
    for ( vector = 0; vector < numberVectors; vector++ )
        for ( i = 0; i < dimension; i++ )
            repairing[vector][i] = [mersennetwister randomDoubleFrom:-1 to:1];
        
}

//! Initialize differential evolution instance
/*!
\param vect Number of vectors to evolve
\param numGenerations How many times will repairing metaevolution (5x) and vectors evolution evolution (5x) take place - one generation is 10 in total so 100 generations will be 1000 evalutations!
\param scalingFactor Differential evolution parameter
\param crossProb probability of the crossver operator
\param defaultProblem pointer to the enum
 
\return id of newly created instance
*/
- (id) initWithVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c defaultProblem:(Problem)p {
    
	if ( self = [super init] ) {
        
        tsp = new cTSP();
        fss = new cFSS();
        
        problem = p;
        
        if (problem == TSP)
            dimension = tsp->GetCities();
        
        else if (problem == FSS)
            dimension = fss->GetJobs();
        
        numberVectors = vect;
		numberGenerations = numGenerations;
        
        scalingFactor = p_m;
		probCrossover = p_c;
        
        bestVectorFitness = FLT_MAX;
        metaEvolutionIndividual = 0;

        mersennetwister = [[MersenneTwister alloc] init];
        
        // allocate all needed memory:
        
        repairingbuffer = (int *)malloc(sizeof(int) * dimension);
        migratingbuffer = (int *)malloc(sizeof(int) * dimension);
        bestbuffer= (int *)malloc(sizeof(int) * dimension);
        
        //fitnesses = (double *)malloc(sizeof(double) * numberVectors);
        //metaFitnesses = (double *)malloc(sizeof(double) * numberVectors);
        
		Xu = (double *)malloc(sizeof(double) * dimension);
        Xl = (double *)malloc(sizeof(double) * dimension);
        
        vectors = (double **) malloc(sizeof(double*) * numberVectors);
		vectors[0] = (double *) malloc(sizeof(double) * dimension * numberVectors);
        
        repairing = (double **) malloc(sizeof(double*) * numberVectors);
		repairing[0] = (double *) malloc(sizeof(double) * dimension * numberVectors);
        
        repaired = (int **) malloc(sizeof(int*) * numberVectors);
		repaired[0] = (int *) malloc(sizeof(int) * dimension * numberVectors);
        
        migratingVectors = (int **) malloc(sizeof(int*) * numberVectors);
		migratingVectors[0] = (int *) malloc(sizeof(int) * dimension * ALL_TO_ALL_MIGRATIONS);
        
        averageFitness = (double *) malloc(sizeof(double) * numberGenerations);
		//bestFitnesses = (double *) malloc(sizeof(double) * numberGenerations);
		normalizedFitnessValues = (double *) malloc(sizeof(double) * numberVectors);
		runningTotal = (double *) malloc(sizeof(double) * numberVectors);
        
        trialRepair = (double *) malloc(sizeof(double) * dimension);
        trialVector = (double *) malloc(sizeof(double) * dimension);
        
        bestFitness = FLT_MAX;
        
        for ( i = 0; i < numberVectors; i++ ) {
			if(i) {
                repairing[i] = repairing[i-1] + numberVectors;
                repaired[i] = repaired[i-1] + numberVectors;
                vectors[i] = vectors[i-1] + numberVectors;
			}
		}
        
        [self initXu];
        [self initXl];
        
        [self randomVectors];
        [self randomRepairing];
        
        for (i=0; i<numberVectors;i++)
            [self repair:i trial:NO];
        
        buffer = [[NSMutableString alloc]init];
        
	} return self;
}

-(BOOL)_isinarray:(int*)array len:(int)length element:(int)element{
    BOOL is = NO;
    for (int i=0; i<length; i++)
        if (array[i] == element) {
            is = YES;
            break;
        }
    return is;
}

//! Evolve by differential evolution
/*!
 changes vectors array or repairing array according to variable metaEvolutionIndividual
 */
-(void)evolve {
    
    // MUTATION:
    
    // find random r1,r2,r3 in [0,numberVectors) such that r1 != j !r2 != r3 :
    while (true) {
        r1=[mersennetwister randomUInt32From:0 to:numberVectors-1];
        if (r1!=j)
            break;
    }
    
    while (true) {
        r2=[mersennetwister randomUInt32From:0 to:numberVectors-1];
        if ((r2!=r1) && (r2!=j))
            break;
    }
    
    while (true) {
        r3=[mersennetwister randomUInt32From:0 to:numberVectors-1];
        if ((r3!=r2) && (r3!=r1) && (r3!=j))
            break;
    }
    
    if (metaEvolutionIndividual)
        for (k = 0; k < dimension; k++)
            trialRepair[k] = repairing[r3][k] + scalingFactor * ((repairing[r1])[k]-(repairing[r2])[k]);
    else
        for (k = 0; k < dimension; k++)
            trialVector[k] = (vectors[r3])[k] + scalingFactor * ((vectors[r1])[k]-(vectors[r2])[k]);
        
    
    // CROSSOVER:
    
    int n = int([mersennetwister randomDoubleFrom:0 to:1] * dimension);
    int l = 0;
    
    while (true) {
        
        l += 1;
        if (([mersennetwister randomDoubleFrom:0 to:1] > probCrossover)
            || (l > dimension))
            break;
    }
    
    if (metaEvolutionIndividual)
        for (k = 0; k < dimension; k++)
            for (kk = n; ((kk > n) && (kk < (n+l))); kk++)
                
                if (k != (kk % dimension))
                    trialRepair[k]=repairing[metavector][k];
    else
        for (k = 0; k < dimension; k++)
            for (kk = n; ((kk > n) && (kk < (n+l))); kk++)
                
                if (k != (kk % dimension))
                    trialVector[k] = vectors[vector][k];
    
    // boundaries check:
    if (metaEvolutionIndividual)
        for (k = 0; k < dimension; k++)
            while ((trialRepair[k] < -1) || (trialRepair[k] > 1)) {
                if (trialRepair[k] < -1)
                    trialRepair[k] = (2*(-1))-trialRepair[k];
                if (trialRepair[k] > 1)
                    trialRepair[k] = (2*1)-trialRepair[k];
            }
    else
        for (k = 0; k < dimension; k++)
            while ((trialVector[k] < Xl[k]) || (trialVector[k] > Xu[k])) {
                if (trialVector[k] < Xl[k])
                    trialVector[k] = 2 * Xl[k] - trialVector[k];
                if (trialVector[k] > Xu[k])
                    trialVector[k] = 2 * Xu[k] - trialVector[k];
            }
}

/*! Generating valid permutations from chandom array

 Thanks to randomization of vector it is possible to escape local extreme!
 
 */
/*!
 \param v index to the repaired[][] array
 \param trial flag to repair repairingbuffer array instead of repaired[][] one
 */
-(void)repair:(int) v trial:(BOOL)trial{

    //?? REPAIRING RANDOMIZATION 1 - put random value as the first element:
    
    //repaired[v][0] = [mersennetwister randomUInt32From:0 to:dimension];//OFF
    repaired[v][0]=vectors[v][0];
    
    for (imeta = 1; imeta < dimension; imeta++) {
    
        if (trial)
            a = repairingbuffer[imeta];
        else
            a = (int)vectors[v][imeta];
        
        repaired[v][imeta] = -1;
        direction = repairing[v][imeta];
            
        while ([self _isinarray:repaired[v] len:imeta element:a]) {
            
            //?? REPAIRING RANDOMIZATION 2 - change direction randomly:
            //direction =[mersennetwister randomDouble0To1Exclusive];// COMMENT TO OFF
            
            if (direction > 0) {
                
                if (a == dimension)
                    
                    //?? REPAIRING RANDOMIZATION 3 - change range of random generator:
                    //a =[mersennetwister randomUInt32From:0 to:[mersennetwister randomDoubleFrom:0 to:dimension]];
                    a =[mersennetwister randomUInt32From:0 to:dimension];// PFF
                else
                    a += 1;
                
            } else {
                
                if (a == 0)
                    
                    //?? REPAIRING RANDOMIZATION 3
                    //a = [mersennetwister randomUInt32From:1 to:[mersennetwister randomDoubleFrom:0 to:[mersennetwister randomDoubleFrom:0 to:dimension]]];
                    
                    a = [mersennetwister randomUInt32From:1 to:[mersennetwister randomDoubleFrom:0 to:dimension]];// OFF
                else
                    a -= 1;
            }
        }
    
        repaired[v][imeta] = a;
    }
    
    // Save changes from the buffer 'repaired' to needed input array:
    for (imeta = 0; imeta < dimension; imeta++)
        
        // We were repairing repairingbuffer so we have to wri
        if (trial)
            repairingbuffer[imeta] = repaired[v][imeta];
    
        // This is crucial: copying repaired member to base vectors array:
        else
            vectors[v][imeta] = (double)repaired[v][imeta];
    
}

+(void)freeMigratingMetaEvolutions: (NSMutableArray*)evolutions{
    
    DE *d;
    
    for (DE* de in evolutions){
        d=de;
        [de free];
    }

    free([d migratingVectors][0]);
    free([d migratingVectors]);
    
    d = Nil;
}

+(NSMutableDictionary *) evolveMigratingMetaEvolutions:(NSMutableArray*)evolutions{
    
    int i=0;
    NSMutableDictionary * oneGenerationStats = [[NSMutableDictionary alloc]init];
    
    for (DE* de in evolutions){
        /*
        [de metaEvolve];
        [de repair:0 trial:NO];
        */
        //int** migrations = &[de migratingVectors][0];
        
        //for (int j=0; j<70; j+=1)
        //    migrations[i][j]=[de migratingVectors][i][j];
        
        [oneGenerationStats setValue:[NSNumber numberWithFloat:[de bestVectorFitness]] forKey:[NSString stringWithFormat:@"%i.bestFitness",i]];
        
        [oneGenerationStats setValue:[de bestDescription] forKey:[NSString stringWithFormat:@"%i.bestDescription",i]];
        
        [oneGenerationStats setValue:[de allVectorsDescription]
            forKey:[NSString stringWithFormat:@"%i.allVectorsDescription",i]];
    
        i+=1;
    }
    
    return oneGenerationStats;
}

+(NSMutableArray *) createMigratingMetaEvolutionsWithDimensionTSP:(int)dim numberVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c{
    
    int i=0;
    //cTSP* tsp = new cTSP();
    //cFSS* fss = new cFSS();
    
    NSMutableArray *evolutions=[[NSMutableArray alloc]init];
    
    for (i=0; i<ALL_TO_ALL_MIGRATIONS; i++)
        [evolutions addObject:[DE newWithVectors:vect generations:numGenerations scalingFactor:p_m crossProb:p_c defaultProblem:TSP]];
    /*
    int **migrations = (int **) malloc(sizeof(int*) * (g->GetCities()));
    migrations[0] = (int *) malloc(sizeof(int) * (g->GetCities()) * ALL_TO_ALL_MIGRATIONS);
    */
    //for (DE* de in evolutions)
        
        //[de setMigratingVectors:migrations];
    
    return evolutions;
}

+(NSMutableArray *) createMigratingMetaEvolutionsWithDimensionFSS:(int)dim numberVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c{
    
    int i=0;
    cTSP* tsp = new cTSP();
    cFSS* fss = new cFSS();
    
    NSMutableArray *evolutions=[[NSMutableArray alloc]init];
    
    for (i=0; i<ALL_TO_ALL_MIGRATIONS; i++)
        [evolutions addObject:[DE newWithVectors:vect generations:numGenerations scalingFactor:p_m crossProb:p_c defaultProblem:FSS]];
    /*
     int **migrations = (int **) malloc(sizeof(int*) * (g->GetCities()));
     migrations[0] = (int *) malloc(sizeof(int) * (g->GetCities()) * ALL_TO_ALL_MIGRATIONS);
     */
    //for (DE* de in evolutions)
    
    //[de setMigratingVectors:migrations];
    
    return evolutions;
}

//! Evolve vectors and metaevolve its repairing individuals
//!
-(void)metaEvolve {
    
    beforeFitness = FLT_MAX;
    
    beforeMeta = 0;
    solutions = 0;
    
    // start evolving:
    for (generation = 0; generation < numberGenerations; generation++){
        
        // we have to evolve each vector by [self evolve] * METAEVOLUTION times causing improved repairing mask for evaluation and saving to 'repaired' array:
        
        for ( vector = 0; vector < numberVectors; vector++ ) {
            
            // turn off metavolution (we need to compute 'beforeMeta' variable
            generationswithnochange = 0;
            
            // but vector have to be reoaired first:
            [self repair:vector trial:NO];
            
            // than evaluated according to problem solved:
            if (problem == TSP)
                beforeMeta = tsp->TourCost(&repaired[vector][0]);
            
            else if (problem == FSS)
                beforeMeta = fss->Makespan(&repaired[vector][0]);
            
            // reset current metaevolution best fitness value for maximum finding:
            metaBestFitness = beforeMeta;
            
            // set metaevolution for repairing particular vector:
            metaEvolutionIndividual = vector;
            
            // reset improvements because now we are counting metaevolution improvements:
            improvements = 0;
            
            // iterate repairing vector metaevolution:
            for (i = 0; i < METAEVOLUTION_ITERATIONS; i+=1) {
                
                // run repairing for every metavector
                for ( metavector = 0; metavector < numberVectors; metavector++ ) {
                    
                    // repair vector without
                    [self repair:vector trial:NO];
                
                    if (problem == TSP)
                        before = tsp->TourCost(&repaired[metavector][0]);
                    
                    else if (problem == FSS)
                        before = fss->Makespan(&repaired[metavector][0]);
                    
                    // evolve repairing vectors:
                    [self evolve];
                    
                    // copy current repairing to the global repairing array:
                    for (imeta = 0; imeta < dimension; imeta++)
                        repairing[vector][imeta] = trialRepair[imeta];
                    
                    // repair:
                    [self repair:metavector trial:NO];
            
                    // evaluate:
                    if (problem == TSP)
                        after = tsp->TourCost(&repaired[metavector][0]);
                    
                    if (problem == FSS)
                        after = fss->Makespan(&repaired[metavector][0]);
                    
                    // selection:
                    if (after < before) {
                        
                        // increment improvements counter:
                        improvements += 1;
                        
                        // if new best repairing vector was found:
                        if (after < metaBestFitness) {
                            
                            for (imeta = 0; imeta < dimension; imeta++)
                                repairing[metavector][imeta] = repaired[metavector][imeta];
                            
                            // and set new maximum value:
                            metaBestFitness = after;
                        }
                    }
                }
            }
            
            //NSLog(@"metavector %i. was improved %i times: (before:%.0f after:%.0f)",vector,improvements,beforeMeta,metaBestFitness);
            
            // reset improvements because now we are counting GA improvements:
            
            metaEvolutionIndividual = 0;
            
            for (i = 0; i < EVOLUTION_ITERATIONS; i+=1) {
                
                improvements = 0;
                solutions = 0;
                
                // run repairing for every metavector
                for ( metavector = 0; metavector < numberVectors; metavector++ ) {
                    
                    // set repairing buffer as current metavector:
                    for (imeta = 0; imeta < dimension; imeta++)
                        repairingbuffer[imeta]=vectors[metavector][imeta];
                    
                    // repair:
                    [self repair:metavector trial:YES];
                    
                    if (problem == TSP)
                        before = tsp->TourCost(repairingbuffer);
                    
                    if (problem == FSS)
                        before=fss->Makespan(repairingbuffer);
                    
                    // evolve repairing vectors:
                    [self evolve];
                    
                    // copy current repairing to the global repairing array:
                    for (imeta = 0; imeta < dimension; imeta++) 
                        repairingbuffer[imeta] = trialVector[imeta];
                    
                    // repair:
                    [self repair:metavector trial:YES];
                    
                    // evaluate:
                    
                    if (problem == TSP)
                        after = tsp->TourCost(repairingbuffer);
                    
                    if (problem == FSS)
                        after=fss->Makespan(repairingbuffer);

                    // selection:
                    if (after < before) {
                        
                        // copy current repairing to the global repairing array:
                        for (imeta = 0; imeta < dimension; imeta++)
                            vectors[metavector][imeta] = repairingbuffer[imeta];
                        
                        if (after < metaBestFitness) {
                            metaBestFitness= after;
                            
                            // increment improvements counter:
                            improvements += 1;
                            
                            // if new best repairing vector was found:
                            if (after < bestVectorFitness) {
                                
                                for (imeta = 0; imeta < dimension; imeta++) {
                                    
                                    bestFitness = after;
                                    migratingbuffer[imeta] = repaired[metavector][imeta];
                                    bestbuffer[imeta] = migratingbuffer[imeta];
                                    
                                    repairingbuffer[imeta] = repairing[metavector][imeta];
                                }
                                
                                solutions += 1;
                            
                                // and set new maximum value:
                                bestVectorFitness = after;
                            }
                        }
                    }
                }
            }
            //NSLog(@"vector %i. was improved %i times: (before:%.0f after:%.0f)",vector,improvements,metaBestFitness,bestVectorFitness);
        }
        
        NSLog(@"\n\n%@\n",[self allVectorsDescription]);
        NSLog(@"%i. generation :\n%@\n\n",generation,[self bestDescription],bestFitness);
        
        if (improvements < AT_LEAST_IMPROVEMENTS_RESETTING_MIGRATION) {
            [self resetMigrate];
        }
        
        generationswithnochange += 1;
        
        if (bestVectorFitness < bestFitness) {
            beforeFitness = bestVectorFitness;
            generationswithnochange = 0;
        }
        
        if (generationswithnochange == GENERATIONS_WITH_NO_CHANGE_RESETTING_MIGRATION){
            [self resetMigrate];
            generation=0;
        }
        
        if ( problem == TSP && bestVectorFitness < TSP_BEST_FITNESS_EVER) {
            NSException *e = [NSException
                              exceptionWithName:@"newest extreme ever found!"
                              reason:@"if (bestVectorFitness < BESTFITNESSEVER)"
                              userInfo:nil];
            @throw e;
        }
    
        if ( problem == FSS && bestVectorFitness < FSS_BEST_FITNESS_EVER) {
            
            NSException *e = [NSException
                              exceptionWithName:@"newest extreme ever found!"
                              reason:@"if (bestVectorFitness < BESTFITNESSEVER)"
                              userInfo:nil];
            @throw e;
            
        }
    }
    
}



//! "@" representation method for the NSLog
/*!
 Prints repaired Xu.
 */
-(NSString *) description  {
    mutableout=[NSMutableString stringWithFormat:@""];
    buffer=[NSMutableString stringWithFormat:@""];
    
    for (int v = 0; v < dimension; v++)
        [buffer appendString:[NSString stringWithFormat:@"%i,",(int)migratingbuffer[v]]];
    
    
    [mutableout appendString:[NSString stringWithFormat:@"Best solution is:\n%@\nFitness = %.0f,",buffer,bestVectorFitness]];
    
    return mutableout;
}

-(NSString *) bestDescription  {
    mutableout=[NSMutableString stringWithFormat:@""];
    buffer=[NSMutableString stringWithFormat:@""];
    
    for (int v = 0; v < dimension; v++)
        [buffer appendString:[NSString stringWithFormat:@"%i,",(int)bestbuffer[v]]];
    
    
    [mutableout appendString:[NSString stringWithFormat:@"Best solution is:\n%@\nFitness = %.0f,",buffer,bestFitness]];
    
    return mutableout;
}

-(NSString *) allVectorsDescription  {
    mutableout=[NSMutableString stringWithFormat:@""];
    int v,x;
    for (v = 0; v < numberVectors; v++) {
        buffer=[NSMutableString stringWithFormat:@""];
    
        for (x = 0; x < dimension; x++)
            [buffer appendString:[NSString stringWithFormat:@"%.0f,",vectors[v][x]]];
        
        [mutableout appendString:[NSString stringWithFormat:@"v[%i] = %@\n",v,buffer]];
        
        buffer=[NSMutableString stringWithFormat:@""];
        
        for (x = 0; x < dimension; x++)
            [buffer appendString:[NSString stringWithFormat:@"%.0f,",repairing[v][x]]];
        
        [mutableout appendString:[NSString stringWithFormat:@"repairing[%i] = %@\n",v,buffer]];
        
        buffer=[NSMutableString stringWithFormat:@""];
        
        for (x = 0; x < dimension; x++)
            [buffer appendString:[NSString stringWithFormat:@"%i,",repaired[v][x]]];
        
        [mutableout appendString:[NSString stringWithFormat:@"repaired[%i] = %@\n",v,buffer]];
    }
    return mutableout;
}



//! "Post-ARC" alternative to dealoc
/*!
 Clears the memory.
 */
- (void) free {
    free(bestbuffer);
	free(Xu);
	free(Xl);
    free(repairingbuffer);
    free(migratingbuffer);
    free(vectors[0]);
    free(vectors);
    free(repaired[0]);
    free(repaired);
    free(repairing[0]);
    free(repairing);
	free(averageFitness);
	//free(bestFitnesses);
	free(fitnesses);
    free(metaFitnesses);
	free(normalizedFitnessValues);
	free(runningTotal);
    free(trialRepair);
    free(trialVector);
}

@end
