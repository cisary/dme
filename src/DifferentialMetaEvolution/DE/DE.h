/*!
 * \file DE.h
 * \brief DifferentialEvolution
 * \author Bc. Michal Cisarik
 * \date 8/18/13
 *
 * Copyright (c) 2013 Michal Cisarik. All rights reserved.
 */

#import "../MersenneTwister/MersenneTwister/MersenneTwister.h"
#import "TSP.h"
#import "FSS.h"

//! C++ includes:
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

typedef enum {
    TSP,
    FSS
} Problem;

///! DEEE
/*!
 \class DE
 \brief Differential Evolution
 */
@interface DE : NSObject {
    
    Problem problem;
    
	// statistics:
	double* averageFitness;
	double* bestFitnesses;
	double* fitnesses;
    double* metaFitnesses;
	double* normalizedFitnessValues;
	double* runningTotal;
    
	double totalFitness;
	double  bestFitness;
    
	// properties of differential evolution:
	int dimension;
	int numberGenerations;
	int numberVectors;
    
	double probCrossover;
    double scalingFactor;
    
    // matrix arrays pointers for malloc
	double **vectors;
    double **repairing;
    
    int **repaired;
    int **migratingVectors;
    
    // vector pointers for malloc:
	double *Xu;
    double *Xl;
    double *trialVector;
    double *trialRepair;
    
    int *bestbuffer;
    int *migratingbuffer;
    int *repairingbuffer;
    
    int bestVectorIndex;
    float bestVectorFitness,metaBestFitness,direction,before,after,beforeFitness,beforeMeta;
    
    // if set to 0 evolve method evolve normally
    // else calculate repairing array for particular individual (metaevolution)
    int metaEvolutionIndividual;
    
    NSMutableString *buffer;
    NSMutableString *mutableout;
   
    // helper variables:
    int vector,metavector,i,j,imeta,a,metaBestIndex,generation,r1,r2,r3,k,kk,improvements,generationswithnochange,solutions;
    BOOL best,bestMeta;
    
    // my custom objects:
    cTSP *tsp;
    cFSS *fss;
    
    // random generator:
    MersenneTwister *mersennetwister;
}

///! A class constructor - factory:
/*!
 Constructs the DE class, and assigns the values.
 */
+ (id) newWithVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c defaultProblem:(Problem)p;

//! Initializers:
/*!
 Constructs the DE class, and assigns the values.
 */
- (id) initWithVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c defaultProblem:(Problem)p;

-(void)initXu;
-(void)initXl;


// internal methods:
-(NSString*) description;
-(void) free;
-(void) seed;
-(void) evolve;
-(void) metaEvolve;
-(void) resetMigrate;
-(void) randomVectors;
-(void) randomRepairing;
-(void) repair:(int) v trial:(BOOL)trial;

-(NSString *) bestDescription;
-(NSString *) allVectorsDescription;

+(NSMutableArray *) createMigratingMetaEvolutionsWithDimensionTSP:(int)dim numberVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c;

+(NSMutableArray *) createMigratingMetaEvolutionsWithDimensionFSS:(int)dim numberVectors:(int)vect generations:(NSInteger)numGenerations scalingFactor:(double)p_m crossProb:(double)p_c;

+(NSMutableDictionary *) evolveMigratingMetaEvolutions:(NSMutableArray*)evolutions;

+(void)freeMigratingMetaEvolutions:(NSMutableArray*)evolutions;

// helper methods:
-(BOOL)_isinarray:(int*)array len:(int)length element:(int)element;

// properites:

@property(readwrite) int metaEvolutionIndividual;
@property(readonly) int** repaired;
@property(readwrite) int** migratingVectors;
@property(readonly) float bestVectorFitness;
@property(readonly) int* migratingbuffer;
@property(readonly) int* bestbuffer;
@property(readonly) double* averageFitness;
@property(readonly) double* bestFitnesses;

@end