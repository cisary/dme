//
//  TSPTests.m
//  TSPTests
//
//  Created by Michal Cisarik on 10/14/13.
//  Copyright (c) 2013 Michal Cisarik. All rights reserved.
//

#import <XCTest/XCTest.h>

#include "TSP.h"

#include <iostream>
#include <fstream>

using namespace std;

ifstream in("in.txt");
ofstream out("out.txt");


@interface TSPTests : XCTestCase

@end

@implementation TSPTests

- (void)setUp
{
    [super setUp];
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    int array[70]={59,44,26,31,1,0,2,4,14,27,24,60,32,33,3,5,29,6,28,7,15,8,10,17,19,16,20,18,13,9,21,11,12,22,23,25,30,34,35,36,37,40,51,38,39,41,42,43,56,47,48,57,55,54,53,52,58,49,61,62,63,45,50,64,65,66,67,68,69,70};
    // 52182
    
        
    //! Initialization of the TSP class
    cTSP* tsp = new cTSP();
    
    float result = tsp->TourCost(array);
    
    //! Calculate a simple tour
    tsp->TourFunction(tsp);
}

@end
/*

 int array[70]={28,27,46,45,3,51,35,40,59,39,68,11,8,34,14,15,7,25,4,13,9,36,2,1,21,33,22,6,10,12,18,19,16,20,17,5,23,24,26,29,30,31,41,32,37,38,42,43,0,44,47,48,49,50,52,54,53,56,55,57,58,63,60,61,62,64,65,66,69,67};
 //54757
 
 */
