//
//  SampleCSV.m
//  FourCSV
//
//  Created by Matthias Bartelmeß on 13.01.10.
//  Copyright 2010 fourplusone. All rights reserved.
//

#import "FPOCsvWriter.h"
#import <Foundation/Foundation.h>

int main (int argc, const char * argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

    NSFileHandle * handle = [NSFileHandle fileHandleWithStandardOutput];
    
    FPOCsvWriter * writer = [[FPOCsvWriter alloc] initWithFileHandle:handle];
    
    [writer writeRow:[NSArray arrayWithObjects:@"Hello \n World", @"Hey", @"\"Good morning\", she said", nil]];
    [writer writeRow:[NSArray arrayWithObjects:@"I can haz üñîçø∂é", @"...", @"New\nline", nil]];
    
    [handle closeFile];
    [pool drain];
    return 0;
}